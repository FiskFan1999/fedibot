package main

import (
	"fmt"
	"github.com/ergochat/irc-go/ircmsg"
)

const (
	Prefix = '!'
)

func OnPrivMsg(e ircmsg.Message) {
	if len(e.Params) < 2 {
		return
	}
	text := e.Params[1]
	target := e.Params[0]
	go func() {
		if text[0] != Prefix {
			return
		}
		irc.Privmsg(target, "fedibot")
		irc.Privmsg(target, fmt.Sprintf("%s", text))
		irc.Privmsg(target, fmt.Sprintf("%+v", e.Source))
		irc.Privmsg(target, fmt.Sprintf("%+v", e.Command))
		irc.Privmsg(target, fmt.Sprintf("%+v", e.Params))
	}()
}
